import React from "react";
import AllCar from "../components/AllCar";

function Car() {
    return (
        <div className="grid grid-cols-fit-96 justify-center gap-4">
            <AllCar />
        </div>
    );
}

export default Car;

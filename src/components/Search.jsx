import React from "react";

const Search = () => {
    return (
        <div className="rounded-full border border-neutral-200 py-2 px-4 dark:border-neutral-700">
            <span className="dark:text-neutral-300">Search here</span>
        </div>
    );
};

export default Search;
